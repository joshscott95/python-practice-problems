# Write a class that meets these requirements.
#
# Name:       Student
#
# Required state:
#    * name, a string
#
# Behavior:
#    * add_score(score)   # Adds a score to their list of scores
#    * get_average()      # Gets the average of the student's scores
#
# Example:
#    student = Student("Malik")
#
#    print(student.get_average())    # Prints None
#    student.add_score(80)
#    print(student.get_average())    # Prints 80
#    student.add_score(90)
#    student.add_score(82)
#    print(student.get_average())    # Prints 84
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.



class Student:
    def __init__(self, name):
        self.name = name
        self.scores = []
    def add_score(self, score):
        self.score = score
        self.scores.append(score)
    def get_average(self):
        self.sum = 0
        for num in self.scores:
            self.sum += num
        if len(self.scores) == 0:
            return None
        return self.sum / len(self.scores)
    

stud1 = Student("Joshua")
stud1.add_score(99)
stud1.add_score(99)
stud1.add_score(99)
stud1.add_score(91)
print(stud1.get_average())

























        
        