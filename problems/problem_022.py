# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    rainy_workday = ["umbrella", "laptop"]
    sunny_workday = ["laptop", "sunglasses"]
    weekend = ["surfboard"]
    
    if is_workday and is_sunny:
        return sunny_workday
    elif not(is_sunny) and is_workday:
        return rainy_workday
    else:
        return weekend

is_workday = True
is_sunny = False

print(gear_for_day(is_workday, is_sunny))
    
