# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    if len(values) <= 1:
        return None
    
    max_num = 0
    max_num2 = 0
    for num in values:
        if num >= max_num:
            max_num = num
    for num2 in values:
        if num2 >= max_num2 and num2 < max_num:
            max_num2 = num2
    
    return max_num2



val1 = [1, 2, 3, 4, 5, 6, 6, 8]

print(find_second_largest(val1))
