# Write a class that meets these requirements.
#
# Name:       ReceiptItem
#
# Required state:
#    * quantity, the amount of the item bought
#    * price, the amount each one of the things cost
#
# Behavior:
#    * get_total()          # Returns the quantity * price
#
# Example:
#    item = ReceiptItem(10, 3.45)
#
#    print(item.get_total())    # Prints 34.5


class ReceiptItem:
    def __init__(self, quant, price):
        self.quant = quant
        self.price = price
    def get_total(self):
        return self.quant * self.price
    
item = ReceiptItem(10, 3.45)

print(item.get_total())