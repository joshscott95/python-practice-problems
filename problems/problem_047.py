# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

import re

def check_password(password):
    regex_pattern = "(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$!@])[A-Za-z\d$!@]{6,12}"
    if re.search(regex_pattern, password):
        return "Valid password"
    else:
        return "Invalid password"
    
    
pass1 = "STOPhackn9!"  
    
print(check_password(pass1))
