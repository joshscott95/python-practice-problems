# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
    boundary_range = range(11)
    if x in boundary_range and y in boundary_range:
        return True
    else:
        return False

print(is_inside_bounds(10, 9))
