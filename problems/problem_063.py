# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem

def shift_letters(word):
    shifted_word = ""
    for letter in word:
        if letter == "Z":
            shifted_word += "A"
        elif letter == "z":
            shifted_word += "a"
        else:
            shifted_word += chr(ord(letter) + 1)
    return shifted_word


input1 = "import"
input2 = "ABBA"


print(shift_letters(input1))
print(shift_letters(input2))















