# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    maxNum = 0
    if len(values) == 0:
        return None
    for item in values:
        if item > maxNum:
            maxNum = item
    if len(values) == 0:
        return None
    return maxNum

list1 = []
print(max_in_list(list1))
        
