# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    half_capacity_check = (len(members_list) * 0.50)
    if len(attendees_list) >= half_capacity_check:
        return "Quorum met"
    else:
        return "Quorum not met"

members_list = ["Deb", "Jake", "Michael", "Susan", "Jerome", "Jeb", "Walker"]
attendees_list = ["Deb", "Jake", "Michael"]

print(has_quorum(attendees_list, members_list))

print(len([10,20,3,4]))