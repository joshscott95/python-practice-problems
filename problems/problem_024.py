# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    sum = 0
    for item in values:
        sum += item
    return sum / len(values)
value1 = [1, 4, 7]
print(calculate_average(value1))
