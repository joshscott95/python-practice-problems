# Complete the count_letters_and_digits function which
# accepts a parameter s that contains a string and returns
# two values, the number of letters in the string and the
# number of digits in the string
#
# Examples:
#   * "" returns 0, 0
#   * "a" returns 1, 0
#   * "1" returns 0, 1
#   * "1a" returns 1, 1


def count_letters_and_digits(s):
    value1 = 0
    value2 = 0
    for c in s:
        if c.isdigit():
            value1 += 1
        elif c.isalpha():
            value2 += 1
    result = f"the number of digits in this string is: {value1} \nthe number of letters in this string is: {value2}"
    return result


string1 = "12ee"

print(count_letters_and_digits(string1))
    
